﻿using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Requests.InteractionManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace BotAgent
{
    public class AttachData
    {
        ClientAtributes atributes;
        KeyValueCollection kvc;
        protected ItxLogger logger = ItxLogger.GetLogger();

        public string ReturnAttach(string attachmentName, string InteractionId)
        {
            var requestAttach = RequestGetInteractionProperties.Create(InteractionId);
            var response = ProcessInteractionService.ReturnAttach(requestAttach);
            var ack = response as Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Events.EventInteractionProperties;


            if (ack.Interaction.InteractionUserData.ContainsKey(attachmentName))
            {
                try
                {
                    string atcValue = Convert.ToString(ack.Interaction.InteractionUserData.GetAsString(attachmentName));
                    return atcValue;
                }
                catch (Exception ex)
                {
                    logger.Error($"Error returning Attachment: {ex}");
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public List<string> ReturnPhones(string phone1, string phone2, string InteractionId)
        {
            List<string> phones;

            var returnPhonesRequest = RequestGetInteractionProperties.Create(InteractionId);

            try
            {
                var returnPhonesResponse = ProcessInteractionService.ReturnAttach(returnPhonesRequest);
                var returnPhonesAck = returnPhonesResponse as Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Events.EventInteractionProperties;
                string callPhoneName = phone1;
                string cadPhoneName = phone2;

                if (returnPhonesAck.Interaction.InteractionUserData.ContainsKey(callPhoneName) && returnPhonesAck.Interaction.InteractionUserData.ContainsKey(cadPhoneName))
                {
                    phones = new List<string>
                    {
                        Convert.ToString(returnPhonesAck.Interaction.InteractionUserData.GetAsString(callPhoneName)),
                        Convert.ToString(returnPhonesAck.Interaction.InteractionUserData.GetAsString(cadPhoneName))
                    };
                    return phones;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Error returning Attachment: {ex}");
                return null;
            }
        }

        public void SendClientInformation(string xml, string InteractionId)
        {
            atributes = new ClientAtributes();
            kvc = new KeyValueCollection();

            try
            {
                atributes = DeserializeObject<ClientAtributes>(xml);

                kvc.Add("u_EC", atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.CodigoCliente);
                kvc.Add("NM_FANTASIA", atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.NomeFantasia);
                kvc.Add("NM_RAZAO_SOCIAL", atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.NomeRazaoSocial);
                kvc.Add("WA_SEGMENTO", atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.CodigoSegmento);
                kvc.Add("NU_SO_CPF_CNPJ", atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.NumeroCPF ?? atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.NumeroCNPJ);
                //kvc.Add("CAD_IDENTIFICADO", IsNullOrEmpty(atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.CodigoCliente.ToString()) ? true : false);
                kvc.Add("TEL_CAD", atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.DadosContatoCliente.NumeroTelefoneContato);
                kvc.Add("BANCO_CAD", atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.DadosBancarioCliente.CodigoBanco);
                kvc.Add("AGENCIA_CAD", atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.DadosBancarioCliente.NumeroAgencia);
                kvc.Add("CONTA_CAD", atributes.Body.ConsultarDadosCadastraisClienteResponse.DadosCliente.DadosBancarioCliente.NumeroContaCorrente);

                SendAttach(kvc, InteractionId);
            }
            catch (Exception ex)
            {
                logger.Error($"An error ocurred in SendClientInformation Method: {ex}");
            }
        }

        public void SendAttach(KeyValueCollection kvc, string interactionID)
        {

            var request = RequestChangeProperties.Create(interactionID);
            request.AddedProperties = kvc;
            request.ChangedProperties = kvc;

            try
            {
                var response = ProcessInteractionService.RequestToIXN(request);
                logger.Info($"Response => {response}");
            }
            catch (Exception excp)
            {
                logger.Error($"An error ocurred in Attachment Creating: {excp.ToString()}");
            }
        }

        public Endpoint ReturnActiveChatEndPoint(InteractionServerProtocol ixnProtocol, string InteractionId)
        {
            var requestAttach = RequestGetInteractionProperties.Create(InteractionId);
            try
            {
                var response = ixnProtocol.Request(requestAttach);
                var ack = response as Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Events.EventInteractionProperties;
                string activeChatHost = Convert.ToString(ack.Interaction.InteractionUserData.GetAsString("ChatServerHost"));
                int activeChatPort = Convert.ToInt32(ack.Interaction.InteractionUserData.GetAsString("ChatServerPort"));

                logger.Info($"ActiveEndPoint Host:{activeChatHost.ToString()} : Port:{activeChatPort.ToString()}");

                Endpoint ed = new Endpoint(activeChatHost, activeChatPort);
                return ed;
            }
            catch (Exception e)
            {
                logger.Error($"An error occured in ReturnActiveChatEndpoint Method: {e.Message.ToString()}");
                return null;
            }
        }

        public T DeserializeObject<T>(string xml) where T : new()
        {
            if (string.IsNullOrEmpty(xml))
            {
                return new T();
            }
            try
            {
                using (var stringReader = new StringReader(xml))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    return (T)serializer.Deserialize(stringReader);
                }
            }
            catch (Exception exc)
            {
                logger.Error($"An error ocurred parsing the XML to Object: {exc}");
                return new T();
            }
        }

        public bool IsNullOrEmpty(string x)
        {
            return x != null && x != "" ? true : false;
        }
    }
}
