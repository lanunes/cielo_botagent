﻿using Genesyslab.Platform.WebMedia.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BotAgent.Repositories
{
    public class ChatRepository
    {
        private readonly Dictionary<string, string> chatByInteractionId;

        public ChatRepository()
        {
            this.chatByInteractionId = new Dictionary<string, string>();
        }

        public string[] Keys()
        {
            return chatByInteractionId.Keys.ToArray();
        }

        public void Add(string InteractionID, string chatHost)
        {
            chatByInteractionId.Add(InteractionID, chatHost);
        }

        public long Count()
        {
            return chatByInteractionId.Count;
        }

        public bool ContainsKey(string interactionId)
        {
            return chatByInteractionId.ContainsKey(interactionId);
        }

        public string Get(string interactionId)
        {
            string response = null;
            if (ContainsKey(interactionId))
            {
                response = chatByInteractionId[interactionId];
            }
            return response;
        }

        public void Remove(string interactionId)
        {

            if (ContainsKey(interactionId))
            {
                chatByInteractionId.Remove(interactionId);
            }

        }
        public IEnumerable<string> All()
        {
            return chatByInteractionId.Values;
        }
    }
}
