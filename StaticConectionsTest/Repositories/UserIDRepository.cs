﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BotAgent.Repositories
{
    public class UserIDRepository
    {
        private readonly Dictionary<string, string> userIDByChatHost;

        public UserIDRepository()
        {
            this.userIDByChatHost = new Dictionary<string, string>();
        }

        public string[] Keys()
        {
            return userIDByChatHost.Keys.ToArray();
        }

        public void Add(string chatHost, string UserID)
        {
            userIDByChatHost.Add(chatHost, UserID);
        }

        public long Count()
        {
            return userIDByChatHost.Count;
        }

        public bool ContainsKey(string chatHost)
        {
            return userIDByChatHost.ContainsKey(chatHost);
        }

        public string Get(string chatHost)
        {
            string response = null;
            if (ContainsKey(chatHost))
            {
                response = userIDByChatHost[chatHost];
            }
            return response;
        }
        public IEnumerable<string> All()
        {
            return userIDByChatHost.Values;
        }
    }
}
