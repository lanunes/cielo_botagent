﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace BotAgent
{
    public static class TopShelfService
    {
        internal static void Configure()
        {
            var config = new ConfigurationService();
            HostFactory.Run(configure =>
            {
                configure.Service<ProcessInteractionService> (service =>
                {
                    service.ConstructUsing(s => new ProcessInteractionService(config));
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });
                configure.RunAsLocalSystem();
                configure.SetServiceName(config.ServiceName);
                configure.SetDisplayName(config.ServiceName);
                configure.SetDescription(config.ServiceName);
            });
        }
    }
}
