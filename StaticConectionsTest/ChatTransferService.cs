﻿using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Requests.InteractionManagement;
using Genesyslab.Platform.WebMedia.Protocols;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat;
using BotAgent.Arguments;
using System;

namespace BotAgent
{
    public static class ChatTransferService
    {
        private static ItxLogger log = ItxLogger.GetLogger();

        public static void Execute(string interactionID, string userID, string sessionID,
            InteractionServerProtocol ixnProt,
            BasicChatProtocol chatProt,
            System.Action Before,
            Action<ResponseToTransferChat> OnSuccess
            )
        {
            Before?.Invoke();
            var config = new ConfigurationService();
            var queue = config.Queue;
            log.Info($"Pegando fila configurada: {queue}");
            var rpq = RequestPlaceInQueue.Create(interactionID, queue, ReasonInfo.Create("Left", "Left"));
            log.Info($"Criando requisição para colocar interacao na fila {queue}");
            var repq = ixnProt.Request(rpq);
            log.Info($"Resposta a requisição para colocar interacao na fila {repq}");
            var rrr = Genesyslab.Platform.WebMedia.Protocols.BasicChat.Requests.RequestReleaseParty.Create(sessionID, userID,
                Genesyslab.Platform.WebMedia.Protocols.BasicChat.Action.KeepAlive, MessageText.Create());
            log.Info($"Criando requisição agente bot sair da interacao de chat {rrr}");
            var rerr = chatProt.Request(rrr);
            log.Info($"Resposta a requisição para colocar interacao na fila {rerr}");

            OnSuccess(new ResponseToTransferChat
            {
                Body = new IMessage[] { repq, rerr }
            });
        }
    }
}
