﻿using Genesyslab.Platform.ApplicationBlocks.WarmStandby;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.Standby;
using System;

namespace BotAgent
{
    public class GenesysConectionProtocol<T> : IDisposable where T: ClientChannel
    {
        public T GenesysProtocol { get; set; }

        WarmStandby _warmStandBy;

        public GenesysConectionProtocol(T protocol, Endpoint endpoint)
        {
            _warmStandBy = new WarmStandby(protocol);
            _warmStandBy.Configuration.SetRetryDelay(1000, 2000);
            _warmStandBy.Configuration.SetEndpoints(endpoint);
            _warmStandBy.Configuration.ReconnectionRandomDelayRange = 3000;
            _warmStandBy.Configuration.BackupDelay = 2000;
            _warmStandBy.AutoRestore(true);

            _warmStandBy.ChannelDisconnected += WarmStandBy_ChannelDisconnected;
            _warmStandBy.EndpointTriedUnsuccessfully += WarmStandBy_EndpointTriedUnsuccessfully;
            _warmStandBy.Channel.Opened += OnChannelOpened;
            _warmStandBy.Channel.Closed += OnChannelClosed;
            _warmStandBy.Channel.Error  += OnChannelError;
            _warmStandBy.Channel.Received += OnChannelReceived;
            
        }

        private void WarmStandBy_EndpointTriedUnsuccessfully(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void WarmStandBy_ChannelDisconnected(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public delegate void Received(object sender, EventArgs e);
        public Received EventReceived;

        private void OnChannelReceived(object sender, EventArgs e)
        {
            if (EventReceived != null) this.EventReceived(this, e);
        }

        public delegate void Error(object sender, EventArgs e);
        public Error EventError;

        private void OnChannelError(object sender, EventArgs e)
        {
            if (EventError != null) this.EventError(this, e);
        }

        public delegate void Closed(object sender, EventArgs e);
        public Closed EventClosed;

        private void OnChannelClosed(object sender, EventArgs e)
        {
            if (EventClosed != null) this.EventClosed(this, e);
        }

        public delegate void Opened(object sender, EventArgs e);
        public Opened EventOpened;

        private void OnChannelOpened(object sender, EventArgs e)
        {
            if (EventOpened != null) this.EventOpened(this, e);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~GenesysConectionProtocol()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}

