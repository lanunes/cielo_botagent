﻿using Genesyslab.Platform.Commons.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotAgent.Arguments
{
    public class ResponseToTransferChat
    {
        public IMessage[] Body { get; set; }
    }
}
