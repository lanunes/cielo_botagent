﻿using System.Xml.Serialization;
using System.Collections.Generic;
namespace BotAgent
{
    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class ClientAtributesV5
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public BodyV5 Body { get; set; }
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
    }

    [XmlRoot(ElementName = "codigoCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class CodigoCliente
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "dataAberturaCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class DataAberturaCliente
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "descricaoPOS", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class DescricaoPOS
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "dadosProprietarioCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class DadosProprietarioCliente
    {
        [XmlElement(ElementName = "nomeProprietario", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NomeProprietario { get; set; }
        [XmlElement(ElementName = "numeroCPFProprietario", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NumeroCPFProprietario { get; set; }
        [XmlElement(ElementName = "dataNascimentoProprietario", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string DataNascimentoProprietario { get; set; }
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
    }

    [XmlRoot(ElementName = "nomeFantasia", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class NomeFantasia
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "codigoMoeda", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class CodigoMoeda
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "dadosBancarioCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class DadosBancarioClienteV5
    {
        [XmlElement(ElementName = "codigoBanco", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string CodigoBanco { get; set; }
        [XmlElement(ElementName = "numeroAgencia", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NumeroAgencia { get; set; }
        [XmlElement(ElementName = "numeroContaCorrente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NumeroContaCorrente { get; set; }
        [XmlElement(ElementName = "numeroDACAgencia", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NumeroDACAgencia { get; set; }
        [XmlElement(ElementName = "numeroDACContaCorrente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NumeroDACContaCorrente { get; set; }
        [XmlElement(ElementName = "nomeBanco", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NomeBanco { get; set; }
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
    }

    [XmlRoot(ElementName = "codigoRamoAtividade", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class CodigoRamoAtividade
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "codigoSegmento", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class CodigoSegmento
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "nomeRazaoSocial", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class NomeRazaoSocial
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "numeroCNPJ", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class NumeroCNPJ
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "codigoTipoPessoa", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class CodigoTipoPessoa
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "codigoClasseFaturamento", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class CodigoClasseFaturamento
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "dadosSituacaoFuncionamentoCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class DadosSituacaoFuncionamentoClienteV5
    {
        [XmlElement(ElementName = "codigoSituacaoCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string CodigoSituacaoCliente { get; set; }
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
    }

    [XmlRoot(ElementName = "dataUltimaAlteracao", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class DataUltimaAlteracao
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "dadosContatoEnderecoCorrespondencia", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class DadosContatoEnderecoCorrespondencia
    {
        [XmlElement(ElementName = "nomeContato", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NomeContato { get; set; }
        [XmlElement(ElementName = "numeroDDDTelefoneContato", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NumeroDDDTelefoneContato { get; set; }
        [XmlElement(ElementName = "numeroTelefoneContato", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NumeroTelefoneContato { get; set; }
        [XmlElement(ElementName = "tipoTelefone", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string TipoTelefone { get; set; }
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlElement(ElementName = "nomeEmailContato", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NomeEmailContato { get; set; }
    }

    [XmlRoot(ElementName = "dadosEnderecoContrato", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class DadosEnderecoContrato
    {
        [XmlElement(ElementName = "nomeLogradouro", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NomeLogradouro { get; set; }
        [XmlElement(ElementName = "descricaoComplementoEndereco", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string DescricaoComplementoEndereco { get; set; }
        [XmlElement(ElementName = "nomeBairro", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NomeBairro { get; set; }
        [XmlElement(ElementName = "nomeCidade", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NomeCidade { get; set; }
        [XmlElement(ElementName = "siglaEstado", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string SiglaEstado { get; set; }
        [XmlElement(ElementName = "numeroCEP", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NumeroCEP { get; set; }
        [XmlElement(ElementName = "numeroLogradouro", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public string NumeroLogradouro { get; set; }
        [XmlElement(ElementName = "categoriaEndereco", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public List<string> CategoriaEndereco { get; set; }
        [XmlElement(ElementName = "contatoEndereco", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public List<string> ContatoEndereco { get; set; }
        [XmlElement(ElementName = "telefoneContato", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public List<string> TelefoneContato { get; set; }
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
    }

    [XmlRoot(ElementName = "indicadorBloqueioPagamento", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class IndicadorBloqueioPagamento
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "motivoRetornoDescricao", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
    public class MotivoRetornoDescricao
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
    }

    [XmlRoot(ElementName = "cliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
    public class Cliente
    {
        [XmlElement(ElementName = "codigoCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public CodigoCliente CodigoCliente { get; set; }
        [XmlElement(ElementName = "dataAberturaCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public DataAberturaCliente DataAberturaCliente { get; set; }
        [XmlElement(ElementName = "descricaoPOS", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public DescricaoPOS DescricaoPOS { get; set; }
        [XmlElement(ElementName = "dadosProprietarioCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public DadosProprietarioCliente DadosProprietarioCliente { get; set; }
        [XmlElement(ElementName = "nomeFantasia", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public NomeFantasia NomeFantasia { get; set; }
        [XmlElement(ElementName = "codigoMoeda", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public CodigoMoeda CodigoMoeda { get; set; }
        [XmlElement(ElementName = "dadosBancarioCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public DadosBancarioClienteV5 DadosBancarioCliente { get; set; }
        [XmlElement(ElementName = "codigoRamoAtividade", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public CodigoRamoAtividade CodigoRamoAtividade { get; set; }
        [XmlElement(ElementName = "codigoSegmento", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public CodigoSegmento CodigoSegmento { get; set; }
        [XmlElement(ElementName = "nomeRazaoSocial", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public NomeRazaoSocial NomeRazaoSocial { get; set; }
        [XmlElement(ElementName = "numeroCNPJ", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public NumeroCNPJ NumeroCNPJ { get; set; }
        [XmlElement(ElementName = "codigoTipoPessoa", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public CodigoTipoPessoa CodigoTipoPessoa { get; set; }
        [XmlElement(ElementName = "codigoClasseFaturamento", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public CodigoClasseFaturamento CodigoClasseFaturamento { get; set; }
        [XmlElement(ElementName = "dadosSituacaoFuncionamentoCliente", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public DadosSituacaoFuncionamentoClienteV5 DadosSituacaoFuncionamentoCliente { get; set; }
        [XmlElement(ElementName = "dataUltimaAlteracao", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public DataUltimaAlteracao DataUltimaAlteracao { get; set; }
        [XmlElement(ElementName = "dadosContatoEnderecoCorrespondencia", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public List<DadosContatoEnderecoCorrespondencia> DadosContatoEnderecoCorrespondencia { get; set; }
        [XmlElement(ElementName = "dadosEnderecoContrato", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public List<DadosEnderecoContrato> DadosEnderecoContrato { get; set; }
        [XmlElement(ElementName = "indicadorBloqueioPagamento", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public IndicadorBloqueioPagamento IndicadorBloqueioPagamento { get; set; }
        [XmlElement(ElementName = "motivoRetornoDescricao", Namespace = "http://canonico.cielo.com.br/cadastro/v1")]
        public MotivoRetornoDescricao MotivoRetornoDescricao { get; set; }
    }

    [XmlRoot(ElementName = "codigoSimboloMoeda", Namespace = "http://canonico.cielo.com.br/administracaorecursosfinanceiros/moeda/v1")]
    public class CodigoSimboloMoeda
    {
        [XmlAttribute(AttributeName = "v1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V1 { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Moeda", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
    public class Moeda
    {
        [XmlElement(ElementName = "codigoSimboloMoeda", Namespace = "http://canonico.cielo.com.br/administracaorecursosfinanceiros/moeda/v1")]
        public CodigoSimboloMoeda CodigoSimboloMoeda { get; set; }
    }

    [XmlRoot(ElementName = "response", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
    public class Response
    {
        [XmlElement(ElementName = "cliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
        public Cliente Cliente { get; set; }
        [XmlElement(ElementName = "Moeda", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
        public Moeda Moeda { get; set; }
        [XmlElement(ElementName = "codigoNoHierarquia", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
        public string CodigoNoHierarquia { get; set; }
        [XmlElement(ElementName = "codigoNivelHierarquico", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
        public string CodigoNivelHierarquico { get; set; }
        [XmlElement(ElementName = "pagamentoRejeitado", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
        public string PagamentoRejeitado { get; set; }
        [XmlElement(ElementName = "debitoPendente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
        public string DebitoPendente { get; set; }
    }

    [XmlRoot(ElementName = "consultarDadosCadastraisClienteResponse", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
    public class ConsultarDadosCadastraisClienteResponseV5
    {
        [XmlElement(ElementName = "response", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
        public Response Response { get; set; }
        [XmlAttribute(AttributeName = "tns", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Tns { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class BodyV5
    {
        [XmlElement(ElementName = "consultarDadosCadastraisClienteResponse", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v5/")]
        public ConsultarDadosCadastraisClienteResponseV5 ConsultarDadosCadastraisClienteResponse { get; set; }
        [XmlAttribute(AttributeName = "soap-env", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
    }

}

