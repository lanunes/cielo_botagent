﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotAgent.Arguments
{
    public class RequestPDFArgscs
    {
        public string email { get; set; }

        public string document { get; set; }
    }
}
