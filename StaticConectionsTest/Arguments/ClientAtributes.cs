﻿using System.Xml.Serialization;

namespace BotAgent
{

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class ClientAtributes
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
    }

    [XmlRoot(ElementName = "dadosBancarios", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosBancarios
    {
        [XmlElement(ElementName = "codigoBanco", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoBanco { get; set; }
        [XmlElement(ElementName = "numeroAgencia", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroAgencia { get; set; }
        [XmlElement(ElementName = "numeroContaCorrente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroContaCorrente { get; set; }
    }

    [XmlRoot(ElementName = "dadosTarifa", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosTarifa
    {
        [XmlElement(ElementName = "nomeTipoTarifa", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeTipoTarifa { get; set; }
        [XmlElement(ElementName = "valorTarifa", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string ValorTarifa { get; set; }
    }

    [XmlRoot(ElementName = "dadosProdutoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosProdutoCliente
    {
        [XmlElement(ElementName = "dadosBancarios", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosBancarios DadosBancarios { get; set; }
        [XmlElement(ElementName = "codigoProduto", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoProduto { get; set; }
        [XmlElement(ElementName = "percentualTaxa", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string PercentualTaxa { get; set; }
        [XmlElement(ElementName = "descricaoProduto", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DescricaoProduto { get; set; }
        [XmlElement(ElementName = "dadosTarifa", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosTarifa DadosTarifa { get; set; }
    }

    [XmlRoot(ElementName = "listaDadosProdutoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class ListaDadosProdutoCliente
    {
        [XmlElement(ElementName = "dadosProdutoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosProdutoCliente DadosProdutoCliente { get; set; }
    }

    [XmlRoot(ElementName = "dadosContatoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosContatoCliente
    {
        [XmlElement(ElementName = "nomeContato", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeContato { get; set; }
        [XmlElement(ElementName = "numeroTelefoneContato", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroTelefoneContato { get; set; }
        [XmlElement(ElementName = "numeroFax", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroFax { get; set; }
        [XmlElement(ElementName = "numeroDDDFax", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroDDDFax { get; set; }
        [XmlElement(ElementName = "nomeEmailContato", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeEmailContato { get; set; }
    }

    [XmlRoot(ElementName = "dadosBancarioCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosBancarioCliente
    {
        [XmlElement(ElementName = "codigoBanco", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoBanco { get; set; }
        [XmlElement(ElementName = "numeroAgencia", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroAgencia { get; set; }
        [XmlElement(ElementName = "numeroContaCorrente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroContaCorrente { get; set; }
    }

    [XmlRoot(ElementName = "dadosProprietarioMobile", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosProprietarioMobile
    {
        [XmlElement(ElementName = "nomeProprietario", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeProprietario { get; set; }
        [XmlElement(ElementName = "numeroCPFProprietario", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroCPFProprietario { get; set; }
        [XmlElement(ElementName = "dataNascimentoProprietario", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DataNascimentoProprietario { get; set; }
    }

    [XmlRoot(ElementName = "dadosEnderecoFisicoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosEnderecoFisicoCliente
    {
        [XmlElement(ElementName = "nomeLogradouro", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeLogradouro { get; set; }
        [XmlElement(ElementName = "descricaoComplementoEndereco", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DescricaoComplementoEndereco { get; set; }
        [XmlElement(ElementName = "nomeCidade", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeCidade { get; set; }
        [XmlElement(ElementName = "siglaEstado", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string SiglaEstado { get; set; }
        [XmlElement(ElementName = "numeroCEP", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroCEP { get; set; }
    }

    [XmlRoot(ElementName = "dadosEnderecoCorrespondenciaCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosEnderecoCorrespondenciaCliente
    {
        [XmlElement(ElementName = "nomeLogradouro", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeLogradouro { get; set; }
        [XmlElement(ElementName = "descricaoComplementoEndereco", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DescricaoComplementoEndereco { get; set; }
        [XmlElement(ElementName = "nomeCidade", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeCidade { get; set; }
        [XmlElement(ElementName = "siglaEstado", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string SiglaEstado { get; set; }
        [XmlElement(ElementName = "numeroCEP", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroCEP { get; set; }
    }

    [XmlRoot(ElementName = "dadosSituacaoFuncionamentoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosSituacaoFuncionamentoCliente
    {
        [XmlElement(ElementName = "codigoSituacaoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoSituacaoCliente { get; set; }
        [XmlElement(ElementName = "dataFechamento", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente", IsNullable = true)]
        public string DataFechamento { get; set; }
        [XmlElement(ElementName = "codigoMotivoFechamento", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoMotivoFechamento { get; set; }
    }

    [XmlRoot(ElementName = "dadosContatoAdicional", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosContatoAdicional
    {
        //[XmlElement(ElementName = "numeroDDDTelefoneContato", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        //public string NumeroDDDTelefoneContato { get; set; }
        [XmlElement(ElementName = "numeroTelefoneContato", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroTelefoneContato { get; set; }
    }

    [XmlRoot(ElementName = "dadosEnderecoSuprimento", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosEnderecoSuprimento
    {
        [XmlElement(ElementName = "nomeLogradouro", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeLogradouro { get; set; }
        [XmlElement(ElementName = "descricaoComplementoEndereco", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DescricaoComplementoEndereco { get; set; }
        [XmlElement(ElementName = "nomeCidade", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeCidade { get; set; }
        [XmlElement(ElementName = "siglaEstado", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string SiglaEstado { get; set; }
        [XmlElement(ElementName = "numeroCEP", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroCEP { get; set; }
    }

    [XmlRoot(ElementName = "dadosCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosCliente
    {
        [XmlElement(ElementName = "codigoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoCliente { get; set; }
        [XmlElement(ElementName = "dataAberturaCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DataAberturaCliente { get; set; }
        [XmlElement(ElementName = "codigoCadeia", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoCadeia { get; set; }
        [XmlElement(ElementName = "descricaoPOS", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DescricaoPOS { get; set; }
        [XmlElement(ElementName = "indicadorCessao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorCessao { get; set; }
        [XmlElement(ElementName = "indicadorCartaCircularizacao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorCartaCircularizacao { get; set; }
        [XmlElement(ElementName = "indicadorAntecipacaoAutomatica", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorAntecipacaoAutomatica { get; set; }
        [XmlElement(ElementName = "indicadorAntecipacaoSecuritizacao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorAntecipacaoSecuritizacao { get; set; }
        [XmlElement(ElementName = "listaDadosProdutoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public ListaDadosProdutoCliente ListaDadosProdutoCliente { get; set; }
        [XmlElement(ElementName = "codigoTipoGarantia", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoTipoGarantia { get; set; }
        [XmlElement(ElementName = "nomeFantasia", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeFantasia { get; set; }
        [XmlElement(ElementName = "dadosContatoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosContatoCliente DadosContatoCliente { get; set; }
        [XmlElement(ElementName = "indicadorEcommerce", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorEcommerce { get; set; }
        [XmlElement(ElementName = "codigoMoeda", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoMoeda { get; set; }
        [XmlElement(ElementName = "indicadorMultiBandeira", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorMultiBandeira { get; set; }
        [XmlElement(ElementName = "codigoPCT", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoPCT { get; set; }
        [XmlElement(ElementName = "indicadorSecuratizacao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorSecuratizacao { get; set; }
        [XmlElement(ElementName = "indicadorTrava", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorTrava { get; set; }
        [XmlElement(ElementName = "indicadorMoto", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorMoto { get; set; }
        [XmlElement(ElementName = "dadosBancarioCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosBancarioCliente DadosBancarioCliente { get; set; }
        [XmlElement(ElementName = "dadosProprietarioMobile", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosProprietarioMobile DadosProprietarioMobile { get; set; }
        [XmlElement(ElementName = "percentualAntecipacao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string PercentualAntecipacao { get; set; }
        [XmlElement(ElementName = "descricaoLimite", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DescricaoLimite { get; set; }
        [XmlElement(ElementName = "indicadorVisaVale", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorVisaVale { get; set; }
        [XmlElement(ElementName = "indicadorJuridico", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorJuridico { get; set; }
        [XmlElement(ElementName = "valorTarifaAgendamento", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string ValorTarifaAgendamento { get; set; }
        [XmlElement(ElementName = "quantidadePOS", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string QuantidadePOS { get; set; }
        [XmlElement(ElementName = "indicadorSaldoConsolidado", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorSaldoConsolidado { get; set; }
        [XmlElement(ElementName = "codigoRamoAtividade", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoRamoAtividade { get; set; }
        [XmlElement(ElementName = "codigoBandeira", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoBandeira { get; set; }
        [XmlElement(ElementName = "codigoSegmento", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoSegmento { get; set; }
        [XmlElement(ElementName = "codigoECAssociada", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoECAssociada { get; set; }
        [XmlElement(ElementName = "nomeRazaoSocial", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeRazaoSocial { get; set; }
        [XmlElement(ElementName = "numeroCNPJ", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroCNPJ { get; set; }
        [XmlElement(ElementName = "indicadorCadeia", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorCadeia { get; set; }
        [XmlElement(ElementName = "indicadorTransmissao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorTransmissao { get; set; }
        [XmlElement(ElementName = "codigoTipoPagamento", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoTipoPagamento { get; set; }
        [XmlElement(ElementName = "dadosEnderecoFisicoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosEnderecoFisicoCliente DadosEnderecoFisicoCliente { get; set; }
        [XmlElement(ElementName = "descricaoRamoAtividade", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DescricaoRamoAtividade { get; set; }
        [XmlElement(ElementName = "indicadorRecebimentoSMS", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorRecebimentoSMS { get; set; }
        [XmlElement(ElementName = "dadosEnderecoCorrespondenciaCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosEnderecoCorrespondenciaCliente DadosEnderecoCorrespondenciaCliente { get; set; }
        [XmlElement(ElementName = "codigoCategoriaAntecipacao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoCategoriaAntecipacao { get; set; }
        [XmlElement(ElementName = "codigoPeriodicidadeAntecipacaoAutomatica", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoPeriodicidadeAntecipacaoAutomatica { get; set; }
        [XmlElement(ElementName = "codigoECPrincipal", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoECPrincipal { get; set; }
        [XmlElement(ElementName = "indicadorParcelado", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorParcelado { get; set; }
        [XmlElement(ElementName = "indicadorMobile", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorMobile { get; set; }
        [XmlElement(ElementName = "numeroCPF", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroCPF { get; set; }
        [XmlElement(ElementName = "codigoClasseFaturamento", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente", IsNullable = true)]
        public string CodigoClasseFaturamento { get; set; }
        [XmlElement(ElementName = "dadosSituacaoFuncionamentoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosSituacaoFuncionamentoCliente DadosSituacaoFuncionamentoCliente { get; set; }
        [XmlElement(ElementName = "dataUltimaAlteracao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DataUltimaAlteracao { get; set; }
        [XmlElement(ElementName = "indicadorPiloto", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorPiloto { get; set; }
        [XmlElement(ElementName = "dadosContatoAdicional", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosContatoAdicional DadosContatoAdicional { get; set; }
        [XmlElement(ElementName = "nomeSituacaoAtividadeCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeSituacaoAtividadeCliente { get; set; }
        [XmlElement(ElementName = "dadosEnderecoSuprimento", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosEnderecoSuprimento DadosEnderecoSuprimento { get; set; }
        [XmlElement(ElementName = "nomeSituacaoAtivacaoCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeSituacaoAtivacaoCliente { get; set; }
    }

    [XmlRoot(ElementName = "dadosAfiliacao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class DadosAfiliacao
    {
        [XmlElement(ElementName = "dataCredenciamento", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string DataCredenciamento { get; set; }
        [XmlElement(ElementName = "nomePlaqueta", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomePlaqueta { get; set; }
    }

    [XmlRoot(ElementName = "consultarDadosCadastraisClienteResponse", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
    public class ConsultarDadosCadastraisClienteResponse
    {
        [XmlElement(ElementName = "dadosCliente", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosCliente DadosCliente { get; set; }
        [XmlElement(ElementName = "dadosAfiliacao", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public DadosAfiliacao DadosAfiliacao { get; set; }
        [XmlElement(ElementName = "numeroMatriz", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NumeroMatriz { get; set; }
        [XmlElement(ElementName = "indicadorMultivan", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorMultivan { get; set; }
        [XmlElement(ElementName = "indicadorAlteracaoDiario", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorAlteracaoDiario { get; set; }
        [XmlElement(ElementName = "indicadorAssinaturaArquivo", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorAssinaturaArquivo { get; set; }
        [XmlElement(ElementName = "nomeEmailContatoAdicional", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeEmailContatoAdicional { get; set; }
        [XmlElement(ElementName = "indicadorBloqueioSuprimentos", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorBloqueioSuprimentos { get; set; }
        [XmlElement(ElementName = "codigoGerenteVirtual", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string CodigoGerenteVirtual { get; set; }
        [XmlElement(ElementName = "nomeGerenteVirtual", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string NomeGerenteVirtual { get; set; }
        [XmlElement(ElementName = "indicadorClientePrepago", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorClientePrepago { get; set; }
        [XmlElement(ElementName = "idPlano", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente", IsNullable = true)]
        public string IdPlano { get; set; }
        [XmlElement(ElementName = "insEstadual", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente", IsNullable = true)]
        public string InsEstadual { get; set; }
        [XmlElement(ElementName = "idElegivel", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente", IsNullable = true)]
        public string IdElegivel { get; set; }
        [XmlElement(ElementName = "permiteDuplicar", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente", IsNullable = true)]
        public string PermiteDuplicar { get; set; }
        [XmlElement(ElementName = "tipoConta", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente", IsNullable = true)]
        public string TipoConta { get; set; }
        [XmlElement(ElementName = "indicadorClienteVendas", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorClienteVendas { get; set; }
        [XmlElement(ElementName = "indicadorFranquia", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public string IndicadorFranquia { get; set; }
        [XmlAttribute(AttributeName = "con", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Con { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body
    {
        [XmlElement(ElementName = "consultarDadosCadastraisClienteResponse", Namespace = "http://service.cielo.com.br/cadastro/cliente/cliente/v81/consultardadoscadastraiscliente")]
        public ConsultarDadosCadastraisClienteResponse ConsultarDadosCadastraisClienteResponse { get; set; }
    }
}
