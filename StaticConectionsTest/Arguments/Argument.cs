﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BotAgent.Arguments
{
    [Serializable]
    [DataContract]
    public abstract class Argument
    {
        public virtual string ToRequest()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
