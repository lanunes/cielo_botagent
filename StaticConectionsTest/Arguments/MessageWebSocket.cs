﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BotAgent.Arguments
{

    [Serializable]
    [DataContract]
    public class MessageWebSocket : Argument
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        public override string ToString()
        {
            return $"{nameof(MessageWebSocket)}(" +
                $"{nameof(Id)}: {Id}, " +
                $"{nameof(Message)}: {Message}, " +
                $");";
        }
    }
}
