﻿using System;
using System.Collections.Generic;
using System.Linq;
using BotAgent.Arguments;
using System.Text.RegularExpressions;
using System.Web;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Requests;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Events;
using Genesyslab.Platform.Commons.Collections;
using System.Net;
using System.IO;

namespace BotAgent
{
    public static class HandleMessage
    {
        private static readonly string transfere = "<<TRANSFERE>>";
        private static readonly string finaliza = "<<FINALIZA>>";
        private static readonly string identificacao = @"<<(.*)>>";
        private static readonly string xmlData = @"<<xml|s*>>>";
        private static readonly string method = @"METHOD";
        private static ItxLogger log = ItxLogger.GetLogger();
        private static AttachData ad = new AttachData();
        static readonly Regex identificacaoRegex = new Regex(identificacao);
        static readonly Regex xmlPattern = new Regex(xmlData);
        static readonly Regex Methods = new Regex(method);
        private static readonly ISet<string> protocols = new HashSet<string>
        {
            transfere, finaliza, identificacao, xmlData, method
        };


        public static void HandleIncommingGenesysMessage(EventSessionInfo inGenMessage)
        {
            var messages = new List<MessageInfo>();

            foreach (var item in inGenMessage.ChatTranscript.ChatEventList)
            {

                if (item is MessageInfo msg)
                {
                    messages.Add(msg);
                }
                if (item is NewPartyInfo newparty)
                {
                    if (newparty.UserInfo.UserType == UserType.Client)
                    {
                        log.Info($"New userid received: {newparty.UserId}]");
                        //this.clientID = newparty.UserId;
                    }
                }
                if (item is PartyLeftInfo partyLeftInfo && partyLeftInfo.Reason.Text.Contains("removed during restoration"))
                {
                    log.Info("InteractionRestored");
                    //restoredEvent = true;
                }
            }

            var message = messages.FirstOrDefault();

            //log.Info($"Message received from user-id [{message.UserId}] | Bot-user-id [{UserId}]");
            if (message != null)
            {
                if (!message.UserId.Equals(ProcessInteractionService.GetUserIDInfo(inGenMessage.ChatTranscript.SessionId)))
                {
                    var messageText = message.MessageText.Text.ToLower().Trim();
                    if (messageText != "")
                        HandleOutGoingWSMessage(inGenMessage.ChatTranscript.SessionId, messageText);
                }
                return;
            }

            PartyLeftInfo leftInfo = inGenMessage.ChatTranscript.ChatEventList.GetAsPartyLeftInfo(0);
            if (leftInfo != null)
            {
                ProcessInteractionService.End(message.UserId, false);
            }
        }

        public static void HandleIncommingWSMessage(MessageWebSocket inWSMessage)
        {
            log.Info($"Received => ID => {inWSMessage.Id} | MESSAGE => {inWSMessage.Message}");

            if (!protocols.Any(p => new Regex(p).IsMatch(HttpUtility.HtmlDecode(inWSMessage.Message))))
            {
                if (string.IsNullOrEmpty(inWSMessage.Message.Trim()))
                    log.Info($"Ignoring message, reason: Empty String");
                else
                    HandleOutGoingGenesysMessage(inWSMessage);
            }
            else
            {
                ExecuteProtocollsActions(inWSMessage);
            }
        }

        public static void HandleOutGoingGenesysMessage(MessageWebSocket outGenMessage)
        {
            log.Info($"SendMessageToUser init, msg: {outGenMessage.Message}");
            RequestMessage message = RequestMessage.Create();
            message.Visibility = Visibility.All;
            message.SessionId = outGenMessage.Id;
            message.MessageText = MessageText.Create(string.Format(outGenMessage.Message));

            ProcessInteractionService.SendMessageToGenesys(message);

            log.Info($"Message sent");
        }

        public static void HandleOutGoingWSMessage(string interactionID, string message)
        {
            try
            {
                var msg = new MessageWebSocket
                {
                    Id = interactionID,
                    Message = message.ToLower().Trim()
                };

                log.Info($"Request to websocket created: {msg}");
                ProcessInteractionService.SendMessageToWS(msg);

                //Tratar se a mensagem é enviada devido a uma restauração.
                //if (restoredEvent == false)
                //{
                //    SendMessageToBot(msg);
                //}
                //else
                //    restoredEvent = false;

            }
            catch (Exception e)
            {
                log.Error($"An error occured in HandleOutGoindWSMessage Method: {e.Message}", e);
            }

        }

        private static void ExecuteProtocollsActions(MessageWebSocket result)
        {

            log.Info("Executing ExecuteProtocollsActions init");

            if (result.Message.Equals(finaliza))
            {

                log.Info("Executing kill interaction");
                ProcessInteractionService.End(result.Id, false);
                log.Info("interaction killed");

            }
            else if (result.Message.Equals(transfere))
            {

                log.Info("Executing transfer interaction");
                ProcessInteractionService.Transfer(result.Id);
                log.Info("Interaction transfered");
                ProcessInteractionService.RemoveChatFromRepository(result.Id);

            }
            else if (xmlPattern.IsMatch(HttpUtility.HtmlDecode(result.Message)))
            {

                string[] spliArray = SplitIntoArray(result.Message);
                string xml = (spliArray[1].Remove((spliArray[1].Count()) - 2, 2));

                try
                {
                    ad.SendClientInformation(xml, result.Id);
                }
                catch (Exception ex)
                {
                    log.Info($"Error: {ex.ToString()}");
                }
            }
            else if (Methods.IsMatch(HttpUtility.HtmlDecode(result.Message)))
            {

                log.Info($"Method Command Identified {result.Message}");
                HandleMethodBot(result.Id, result.Message);
            }
            else if (identificacaoRegex.IsMatch(HttpUtility.HtmlDecode(result.Message)))
            {
                log.Info($"Executing attach interaction");
                var attach = identificacaoRegex.Match(HttpUtility.HtmlDecode(result.Message)).Groups[1].Value;
                TreatAttach(attach, result.Id);
                log.Info($"Attached interaction");
            }

            log.Info("ExecuteProtocollsActions completed");
        }

        public static void TreatAttach(string attach, string interactionID)
        {
            string[] attachArray = SplitIntoArray(attach);

            var nomeAttach = attachArray[0];
            var valorAttach = attachArray[1];

            if (nomeAttach == "es_wa_bot_nav")
            {

                var AttachNavegacao = ad.ReturnAttach(nomeAttach, interactionID);
                if (!string.IsNullOrEmpty(AttachNavegacao))
                {
                    valorAttach = string.Concat(AttachNavegacao, ";", valorAttach);
                    log.Info($"Attachment Concatenado {valorAttach}");
                }
            }
            else if (nomeAttach == "ATC_RESPOSTA3" && valorAttach.Length > 1024)
            {
                string substring = nomeAttach.Substring(0, 1024);
                nomeAttach = substring;
            }

            KeyValueCollection kvc = new KeyValueCollection
            {
                { nomeAttach, valorAttach}
            };

            try
            {
                ad.SendAttach(kvc, interactionID);
            }
            catch (Exception ex)
            {
                log.Error($"Error: {ex.ToString()}");
            }
        }
        private static string[] SplitIntoArray(string value)
        {
            string[] stringArray = value.Split('|');
            return stringArray;
        }

        public static void HandleMethodBot(string interactionID, string msg)
        {
            //*
            //**
            // Método utilizado para validações e verificações lógicas no bot.
            // [1] Posição do Array: Nome do método à ser executado.
            // [2] Posição do Array: Valor à ser validado.
            // [3] Posição do Array: Flag informando se é a primeira ou a segunda tentativa no método ValidDocument ou
            //  O nome do Attach a ser comparado no método CheckPhoneNumber.
            //**
            //*

            string[] splitArray = SplitIntoArray(RemoveSpecialChars(HttpUtility.HtmlDecode(msg)));
            var methodName = splitArray[1];
            var firstParameter = splitArray[2];

            if (methodName == "ValidDocument")
            {
                log.Info($"Method Command ValidDocument {msg}");
                bool validDoc = CommandMethods.ValidDocument(firstParameter);
                string tipoDoc = "Invalido";

                HandleOutGoingWSMessage(interactionID, validDoc.ToString());
                if (validDoc)
                {
                    switch (firstParameter.Count())
                    {
                        case 11:
                            tipoDoc = "CPF";
                            break;
                        case 14:
                            tipoDoc = "CNPJ";
                            break;
                        default:
                            tipoDoc = "EC";
                            break;
                    }
                }
                TreatAttach($"{(splitArray[3] == "1" ? "es_wa_tipo_iden" : "es_wa_tipo_iden_2")}|{tipoDoc}", interactionID);
            }
            else if (methodName == "SendValueToBot")
            {
                HandleOutGoingWSMessage(interactionID, firstParameter.ToString());
            }
            else if (methodName == "ValidaNumeroReferencia")
            {
                HandleOutGoingWSMessage(interactionID, CommandMethods.ValidNumReferencia(firstParameter).ToString());
            }
            else if (methodName == "ValidaNumeroLogico")
            {
                HandleOutGoingWSMessage(interactionID, CommandMethods.ValidNumLogico(firstParameter).ToString());
            }
            else if (methodName == "ValidaDataNascimento")
            {
                HandleOutGoingWSMessage(interactionID, CommandMethods.ValidBirthDate(firstParameter).ToString());
            }
            else if (methodName == "AttachedMessage")
            {
                string attachedMessage = ad.ReturnAttach(firstParameter, interactionID);
                if (!string.IsNullOrEmpty(attachedMessage))
                {
                    HandleOutGoingGenesysMessage(new MessageWebSocket { Id = interactionID, Message = attachedMessage });
                }
            }
            else if (methodName == "CheckPhoneNumber")
            {
                var secondParameter = splitArray[3];
                List<string> phoneNumbers;
                try
                {
                    ad = new AttachData();
                    phoneNumbers = new List<string>(ad.ReturnPhones(firstParameter, secondParameter, interactionID));
                    HandleOutGoingWSMessage(interactionID, Convert.ToString(CommandMethods.CheckPhoneNumbers(phoneNumbers.ElementAt(0), phoneNumbers.ElementAt(1))));
                }
                catch (Exception exc)
                {
                    log.Error($"An error occured in CheckPhoneNumber Method: {exc.Message}");
                    HandleOutGoingWSMessage(interactionID, "Error");
                }
            }
            else if (methodName == "CheckAgency")
            {
                HandleOutGoingWSMessage(interactionID, CommandMethods.CheckAgency(firstParameter).ToString());
            }
            else if (methodName == "SendPDF")
            {
                byte[] fileBytes = Convert.FromBase64String("JVBERi0xLjQKJeLjz9MKNCAwIG9iago8PC9GaWx0ZXIvRmxhdGVEZWNvZGUvTGVuZ3RoIDE4Mjk+PnN0cmVhbQp4nJ2azXLbNhSF93oKLJsZGwFA/HbHSHJHGUtKJCXT6XTDWEyqjmy1kp3M9O06XfQ12lWfoPtekGASkSB10UUSx/nO5SV4eHEghxFOrjlhRDkFv9/dj34dvdiMMk2UcmSzHU03o9f+O55hAAtAjSSb+9HzG07gi/ejb8blw+Ox2JNtSVblvrjbHR6Ke/je4dnmZ1+gJde2losgl4wJoqQR5K74ZfdY7E6kJMfyw+7f8kTuy8fj4ZfDHr7/UJx6Corzgswy3yUj1hfdlve+5P5wV+x322Jb9lRR+vyulk8fd9vDcVf04KznokJYS3785vGwLU6kOLvwj8/itaQ8r7UuPzw9bIvr9+XuWJCCnP5+V2wPV8RXtOSvE1yD/HQ4xtbDmK8eUFPv06dP9G5X7g/07nBP3x2D7FfSqDJoWSlFoe7dPXn+/XtOJgdy/uAVaxCobSR1Er4PDrn+6uvzXrhURGW8WVbOqmZmi5vlaj4lr/JVTiaz1Q2ZTxfr/JZck3yxJOP8drqY/L6aLWFpuGtaHTFqlPDXaf15/DCqrsM00Ub55TyWo/fnpq0A/tm1tuqDzPP1GjrIJ8s1yW9mtxQ6EJm55kwYS8aLVy+/JRmn1kqqmHnOGOPX1gE0W0xmb2eTN/nt83m+Wc1+iD5V6RRVzSVNdckV3NkM7nYDV5yS2fzVcr1ZksmU+H/IY1WiZcbLCSg208Wffyx7RLYlsUyqCGqj9SfT9Xg1g+JksiRfeu7Rty+1fII39gST4GG7q6bA6fMzfA2/BHk5YuS71lQBpxrb1NGhi/lysd6s8s3sre9kTd7mt8vVdA3O+Q7+9mp6uyTT9SZ/Mb2djs9aHLqSYlQZIrVrrqbqNYXH3b3BTGaUReiXxQO8mpH5lhlN4W3uCG7Kj+UxLpFC0Ex0JfPi+E8M1xk4v4vn7467fRdXDBwcwefFLlJcZZpmWeR+nx5+iuFwt8zF8H0M15zRSPH8w+EU2yu0FBSGcodfl4/l/bvYUmqbURfpByz5FBUYbiizXcHi8LHnEgZGoIncxKT8rUdhGSxS5AlsDo/FHmFYTr2YnYurV4Fslpv8NmJCxSnTXRG7YixCW0cFFpbC0Eyiaa2otFhasYzqDE1LTo1B08ZRx7G05jZhAbXS+AXUsJvoSCOCusxdORFxXCYSFhH2/4RFNM5RFnmcrW6Gd1/nDSqdDz7UauLqDXhAIaSlkDC9BgaIcAhJJmDZZCXJLIWtHCHRghqXJIEkCjeeJslctUOkSIyhgidJFFfNiqElKgvPBa1wPHWNtXCpa6y1SV1j4ydo2hobKVPX2FgRXeMLk1nx2GQez3w6WdOcRlITRBB/obYSdmpInrBX2DprOt4/2jvaodGOg5vRjqTDaMfRzWhH0mG0I+kw2nF0M9qRdD3akTC8PypSOlNXdmCu44o3cx1HW6aqIIRspXayFOdw+3wyaOrGmO0ig8bEwY0xkXQwJo5ujImkgzGRdDAmjm6MiaRrYyJhJ3sWUGX9vsTVbnyJoy2zPQv4uZMLp8Tss7LvlPgiX4zhj/FyPnRIDsdAOOwkHAPb9MVjYFuAOAa2JReOgW38wjGwW33oGNi53+FjYBcfOgZ2Or9wDGzzl4+BbcXFY2BbcPkY2FZcPga2FefHwKHUzMAjvErZ4FxcyvYWBw08H45N2UZXEh+BkCHbJimqjK2SFD5ipykgYUOAS1D4gC3SFJCv/TuSoIB8IJMUPl4rkaSAdK2TFAaiguFJCpiGNk1hOXUdweCcF63XpJrqZLbJ34zz1YSsY5+WhpnelsLLzR3saYrXudrEAgnEF551tQPxJdLkQH7B0z6/cCzt84vC05BfNBqG+BJZzb70YqqDIJKG6WnxtIWznOrSsF8yd8VjoQQijJ/nuPo+wnD0nRoHY1WmdOOd8uWHD/Fs3e/ItnLYkTi6cSSSDo7E0Y0jkXTtSCRcOxIHN45E0sGRSBomeWT5MnmlZb8bcbUbN+Jof9CTkb6/buVSqtDpoUInZwqdGinwgpAoEgR1oEgQ1HkCLwhxIkFQp4kEQR0m8IKQJRIEdZTAC0KSSBDUQSJBUOUIjY8Ruh22/Q9MvhwNx3nkaBgGb1s6OHdxcBi7SLieujg4DF0kXM1cJFuNXBwbJi4SrgcuEq4TQBu+GABw1cPERcL19o9vRfj/2/I/N/+2cNCDODh4EAnXHsTBwYNIuPIgkq08iGODB5Fw7UEkXO35bXZ4y8dVDv7DwWHDH+rjwido2A/Q1tPOB2jDldufbNR1es3dxgfNjYODuZFwbW4cHMyNhCtzI9nK3Dg2mBsJ1+ZGwtZS2V045VTcsbW9cbWDvXGwZcJnCGwj4AzRehPqLf6Lj3v91xYO+g8HB/8h4dp/ODj4DwlX/kOylf9wbPAfEq79h4TrDb4NC+qYHNrgcdWDA5FwvcFfaOU/53L9qwplbmRzdHJlYW0KZW5kb2JqCjYgMCBvYmoKPDwvQ29udGVudHMgNCAwIFIvVHlwZS9QYWdlL1Jlc291cmNlczw8L1Byb2NTZXQgWy9QREYgL1RleHQgL0ltYWdlQiAvSW1hZ2VDIC9JbWFnZUldL0ZvbnQ8PC9GMSAxIDAgUi9GMiAyIDAgUj4+L1hPYmplY3Q8PC9YZjEgMyAwIFI+Pj4+L1BhcmVudCA1IDAgUi9Sb3RhdGUgOTAvTWVkaWFCb3hbMCAwIDU5NSA4NDJdPj4KZW5kb2JqCjEgMCBvYmoKPDwvU3VidHlwZS9UeXBlMS9UeXBlL0ZvbnQvQmFzZUZvbnQvSGVsdmV0aWNhLUJvbGQvRW5jb2RpbmcvV2luQW5zaUVuY29kaW5nPj4KZW5kb2JqCjIgMCBvYmoKPDwvU3VidHlwZS9UeXBlMS9UeXBlL0ZvbnQvQmFzZUZvbnQvSGVsdmV0aWNhL0VuY29kaW5nL1dpbkFuc2lFbmNvZGluZz4+CmVuZG9iagozIDAgb2JqCjw8L1N1YnR5cGUvRm9ybS9GaWx0ZXIvRmxhdGVEZWNvZGUvVHlwZS9YT2JqZWN0L01hdHJpeCBbMSAwIDAgMSAwIDBdL0Zvcm1UeXBlIDEvUmVzb3VyY2VzPDwvUHJvY1NldCBbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0+Pi9CQm94WzAgMCA3NC45NCAyNC43OF0vTGVuZ3RoIDE3MTY+PnN0cmVhbQp4nIVXSY7ENgy89yt8DhBDKyV9IQ9JgmD6kv8fUqyi5J5JgGAOo3LbFJfilq/fXvn665Uu//v7j1dudylXv8f1fuV6Txxru75eudzravfQGada/JTvjHPiOd3lqvcynNddbZ/n3f397F8OF9Du5mfzn9s9jee6L/Kr+710zHbZ7S/w5qFLcfe8Ji72I8TOe06e07zWPco+QyFeypcAOs/lcqVtywEYAgZQ7qbPFwH1p15wRReYEADxLbTGuReeZ3OwZCYszPiQ5mc/yhO4Cucx6aI2HaxwnR2QXfp+DWjOLQAqVT9TSai0oMu6A/iVK8JS7k5End1EB4pdco8D0e9A7qjlkhi18wuixh8mzdEnQ7blR7TRiOWOpG8IsmJXAKZ7xf3pyk0nxderuvp5uPyvV3HvZXPnQxu/wU2larwHYXEByZ95DHiuHhvKgvrmISyufrpkTjxePICr8256tzhXkuRBh+FOD9k0xM8IeRcjsvuyk2bZ9QvaFjypMovKV0a5XsWZQesmzlVme1RlG4KGYxJV7EoeoPAslBQhoGtSUHDTrzRC0eM7EWaISEEApmqSLmJFpofgzw4dCn063OISJp3k/n7+8/X7Lx8lYIKH8DDkvgn8ejey4NnH0eKlg6pCz9dYO3g2JkyNXxqDSlcB5ZMmJTOBIvhATiyTw4A8zCaaleSutKDzosQhBkBZf3HIQD6ka5k+bvDUS+to/g18d0PNwXRQ6v2qySvLJnRZrANTqi8m7JCPy2TUh4hWxi3Nacdg8tIII/FbZIF5kajuKoDONCjhie6ZBhLoxU5zk7JPaIVEvjjD0cjFzW8Ihx9MJAMYzvCtHEs5C2P39MCtpQklhteTqFFL6MqKW1kbu8QBVeavCq3KjvkbQCTudktT3dk1YLnbJ8XX6UVzl646oqrRF1VlNWpSVfWNalXZEw6qLBEqWLV42dhFCoE0FcpAJ6yMHYrDcqvfByz94CJyeGpRx6KUBBcq6U52Aq1N9+oJCE+x4EALJ2Cndqds1xpEH2GEnTYA+/oDjAndRbg6okOYPMaewlABqCUN+bW3HSqAyc5lPyz9F9P5lXp+ZRmKXlxJpybTULF3269ewpo8WP27GAEUkv1D9Q7WwuusQE2qyDEtAsfSpsJVWX9zfM8KlmRxdc+r9MiBURDdf6qalTrvqgkN8fsH8k/CilNNYR5vCEruu5brndT8m48NWYXHCY1KqtmhqM4PO17vHxH4r7LakG1Xaf7dm8C2CzYgWwGgVMxOAO2qMZQMZ2TRhAGwHHD2aNOjWpSfk3nAkDWSXK7F2YkWzTm5qCQK9qKeQmIAjMeLvfnTqkrUc3RDfI1GL0u+Ps36Ab6b3zsLADvB25GpSVB05ygWCQWUmWw5UCqnx0MjZ0iKiYMoCqBenBoYALzBf0gfR7h5NSzx1vIJl17s9pAd53mG1o6586pxyXAmFU2cfXr9jWbf1QXiyuUjl5S0pM6s3s8IJHnUsto/+WdFMWFdNV7nhDUm0A6IeQQOML1hkQ/8kBP2FskhLsJsGjiVWyOxbqvIDvq2yMEAdohlLM39ALYOWgvQdhvBnWvu3cCYYOop0Mx3hiLFV9tlwjhfB8WND8PVxjxv4biitJhyVT1lyTg9hZ8h1jQqA/iOQJtwzqcbKjRTUweCtnxPoIc2WE8Ak8Y8F+BgheQyTxMG8t69+alGUQ4JD8t/IuXDnVJdnm82S+1eZ1pG6JAhRo5oTHk7KqeVGKfIPTgZKbcbgFUtBXbYspsGnM4GRPOME8BeKxCmNM80ogh6U38CvWcT5025tGyQOKmcSW/QWdxKCAY7Zo2fpp1J5aBUDloqa/osdjgA8xwu80gf6hli7BAzBhewLgVHi/mmCHTPdanX3JCpij24m614rZ9dQc93ox90sZYuIsV2hOxnEZQKO7SjRhtnYgKV0+EHv97DK1CeZ0QaZJTvQuGJFQPr0EI45SJTV4/Z02ZMQTlQvjQFM45Vw9NUxNvZ4axrCVyRgNroNvpYAkGn0s5X5W5nGLM9R+2yNp85yrSJzWgVK7a/HGWycxBUZZ431z9l8gflf6L/S5eSYt+sni7YEfKzigINquplCHtGOetrLCRLAQDCf24UANUvdrFFCFGBoCrgVTHH9C9UYoTcqM7nsyJnhsh6t+eyerYfKIJemefWcTdOaQ+0d6HMdjti+5nfkN/9oHw+U9S2SNFl34bIjEcTp9lRkjvNVl8R3KYdlD5QPntU3c4KeelIH8fFUmN7XyqeyCQxuR+7TjQ/Iv0TkSX/AEwxnUsKZW5kc3RyZWFtCmVuZG9iago1IDAgb2JqCjw8L0tpZHNbNiAwIFJdL1R5cGUvUGFnZXMvQ291bnQgMS9JVFhUKDUuMC42KT4+CmVuZG9iago3IDAgb2JqCjw8L1R5cGUvQ2F0YWxvZy9QYWdlcyA1IDAgUj4+CmVuZG9iago4IDAgb2JqCjw8L01vZERhdGUoRDoyMDE5MTIxOTA4MDAxMy0wMycwMCcpL0NyZWF0aW9uRGF0ZShEOjIwMTkxMjE5MDgwMDEzLTAzJzAwJykvUHJvZHVjZXIoaVRleHQgNS4wLjYgXChjXCkgMVQzWFQgQlZCQSk+PgplbmRvYmoKeHJlZgowIDkKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAyMTEwIDAwMDAwIG4gCjAwMDAwMDIyMDMgMDAwMDAgbiAKMDAwMDAwMjI5MSAwMDAwMCBuIAowMDAwMDAwMDE1IDAwMDAwIG4gCjAwMDAwMDQyMTQgMDAwMDAgbiAKMDAwMDAwMTkxMiAwMDAwMCBuIAowMDAwMDA0Mjc3IDAwMDAwIG4gCjAwMDAwMDQzMjIgMDAwMDAgbiAKdHJhaWxlcgo8PC9JbmZvIDggMCBSL0lEIFs8ZjZhMWFjNWZlMTUxZTU3OGJiOWI2YmQ2MGFmOTczNzQ+PGUzMGE1NmQ4ZmMwMTg0NGRlNzlkMGJmNmE3ODNlOTg3Pl0vUm9vdCA3IDAgUi9TaXplIDk+PgpzdGFydHhyZWYKNDQ1MgolJUVPRgo=");

                ProcessInteractionService.Upload(interactionID, fileBytes);
            }
        }

        public static string ConsultService()
        {

            try
            {
                WebRequest request = WebRequest.Create("http://www.contoso.com/default.html");
                request.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                log.Info($"StatusCode: {response.StatusDescription}");
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                log.Info(responseFromServer);

                reader.Close();
                dataStream.Close();
                response.Close();
                return "";
            }
            catch (Exception)
            {

                throw;
            }
        }




        public static string RemoveSpecialChars(string input)
        {
            return Regex.Replace(input, @"[^0-9a-zA-Z|_/.]", string.Empty);
        }

    }
}
