﻿using log4net;
using System;

namespace BotAgent
{
    public class ItxLogger
    {
        private static ItxLogger _logger;
        public static ItxLogger GetLogger()
        {
            if (_logger == null) _logger = new ItxLogger();

            return _logger;
        }

        private ILog log;

        public ItxLogger()
        {
            log4net.Config.BasicConfigurator.Configure();
            log = log4net.LogManager.GetLogger(typeof(Program));
            //log.Debug("This is a debug message");
            //log.Warn("This is a warn message");
            //log.Error("This is a error message");
            //log.Fatal("This is a fatal message");
        }

        public void Debug(string message)
        {
            log.Debug(message);
            //Console.WriteLine("[DEBUG] " + message);
        }

        public void Info(string message)
        {
            log.Info(message);
            //Console.WriteLine("[DEBUG] " + message);
        }

        public void LogMessage(String message)
        {
            Debug("Message Received: \r\n" + message.Replace("\n", "\r\n") + "\r\n************************************");
        }

        public void LogRequest(String request)
        {
            Debug("Request: \r\n" + request.Replace("\n", "\r\n") + "\r\n************************************");
        }

        public void Error(string message, Exception ex = null)
        {
            log.Error(message, ex);
            //Console.WriteLine("[ERROR] " + message);
            //if (ex != null)
            //    LogException(ex);
        }

        public void LogException(Exception e)
        {
            log.Error(e);
            //Console.Write("[EXCEPTION] \r\n" + e.Message + "\r\n" + e.StackTrace + "\r\n");
        }
    }
}
