﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebSocket4Net;
using System.Text;
using System.Threading.Tasks;
using BotAgent.Arguments;

namespace BotAgent
{
    public class BotConnection
    {
        private static WebSocket BotSocket;

        public static void CreateWebSocket()
        {
            if (BotSocket != null)
                return;

            var url =  new ConfigurationService().WS;
            BotSocket = new WebSocket(url);

            BotSocket.Opened += (e, v) =>
            {
                Console.WriteLine("Chatbot Socket opened");
            };

            BotSocket.Error += (e, v) =>
            {
                Console.WriteLine($"Chatbot Socket error");
            };
            BotSocket.Closed += (e, v) =>
            {
                Console.WriteLine($"Chatbot Socket closed");
            };
            BotSocket.DataReceived += (e, v) =>
            {
                Console.WriteLine("Chatbot Data received");
            };
            BotSocket.MessageReceived += (e, v) =>
            {
                Console.WriteLine("Chatbot Message received");
            };
            BotSocket.Open();
        }

        public static void CloseWebSocket()
        {
            if (BotSocket.State != WebSocketState.Closed && BotSocket.State != WebSocketState.Closing)
            {
                try
                {
                    BotSocket.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to close socket");
                    Console.WriteLine(e);
                }
            }
        }

        public static void Listen(EventHandler<MessageReceivedEventArgs> handler)
        {
            if (BotSocket != null)
                BotSocket.MessageReceived += handler;
        }

        public static void StopListen(EventHandler<MessageReceivedEventArgs> handler)
        {
            if (BotSocket != null)
                BotSocket.MessageReceived -= handler;
        }

        public static void SendMessageToBot(MessageWebSocket message)
        {
            if (BotSocket.State == WebSocketState.Open)
            {
                BotSocket.Send(message.ToRequest());
            }
            else
            {
                //BotSocket.Open();
                BotSocket.Send(message.ToRequest());
            }
        }

        public static WebSocketState BotConnStatus()
        {
            return BotSocket.State;
        }
    }
}
