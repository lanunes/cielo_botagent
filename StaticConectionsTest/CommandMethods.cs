﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BotAgent
{
    public static class CommandMethods
    {
        public static bool CheckPhoneNumbers(string callPhone, string cadPhone)
        {
            try
            {
                string phone1 = callPhone.Substring(callPhone.Length - 8, 8);
                string phone2 = cadPhone.Substring(cadPhone.Length - 8, 8);

                if (phone1 == phone2)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool ValidDocument(string document)
        {
            bool retorno = false;
            if (IsNumber(document))
            {
                if (document.Length == 11)
                {
                    retorno = IsCpf(document);
                }
                else if (document.Length == 14)
                {
                    retorno = IsCnpj(document);
                }
                else if (document.Length <= 10)
                {
                    retorno = true;
                }
            }
            return retorno;
        }

        public static string CheckAgency(string _Agency)
        {
            string agency = Regex.Replace(_Agency, @"[^0-9a-zA-Z]", string.Empty);
            string retorno = "False";
            if (IsNumber(agency))
            {
                if (agency.Length <= 5)
                {
                    if (agency.Length == 4)
                        agency = string.Format("0" + agency);

                    retorno = agency;
                }
            }

            return retorno;
        }

        public static bool ValidNumReferencia(string numero)
        {
            if (IsNumber(numero) && numero.Length <= 7)
            {
                return true;
            }
            else
                return false;
        }
        public static bool ValidNumLogico(string numero)
        {
            if (IsNumber(numero) && numero.Length <= 8)
            {
                return true;
            }
            else
                return false;
        }

        public static bool ValidBirthDate(string date)
        {
            if (date.Length == 10)
            {
                if (date != "01/12/1990" && date != "01.12.1990" && date != "01-12-1990")
                {
                    try
                    {
                        bool birthDateValid = Convert.ToDateTime(date) < DateTime.Today ? true : false;
                        return birthDateValid;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
                return false;
            }
            else
                return false;
        }

        private static bool IsNumber(string expression)
        {
            Int64 myInt;
            bool isNumerical = Int64.TryParse(expression, out myInt);
            return isNumerical;
        }

        private static bool IsCnpj(string cnpj)
        {

            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }

        private static bool IsCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }

        public static bool IsNullOrEmpty(string x)
        {
            return x == null || x == "" ? true : false;
        }
    }
}
