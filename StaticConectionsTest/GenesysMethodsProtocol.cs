﻿using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Events;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Requests.AgentManagement;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Requests.InteractionDelivery;
using Genesyslab.Platform.WebMedia.Protocols;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Requests;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Events;
using System;
using System.Collections.Generic;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Requests.InteractionManagement;
using Genesyslab.Platform.Contacts.Protocols;
using Genesyslab.Platform.Contacts.Protocols.ContactServer.Requests;
using Genesyslab.Platform.Contacts.Protocols.ContactServer.Events;
using System.IO;

namespace BotAgent
{
    public class GenesysMethodsProtocol : IDisposable
    {
        //InteractionServerProtocol _ixnServerProtocol;
        //public GenesysMethodsProtocol(InteractionServerProtocol ObjIxnServer)
        //{
        //    this._ixnServerProtocol = ObjIxnServer;
        //}
        public string UserId { get; set; }
        private static ItxLogger log = ItxLogger.GetLogger();
        private static ConfigurationService config = new ConfigurationService();

        public void AgentLogin(InteractionServerProtocol _ixnServerProtocol)
        {
            try
            {
                RequestAgentLogin request = RequestAgentLogin.Create(config.TenantId, config.Place, null);
                request.AgentId = config.Employee;

                IMessage ack = _ixnServerProtocol.Request(request);
                if (ack is EventAck)
                {
                    AddMedia(_ixnServerProtocol);
                }
            }
            catch (ProtocolException e)
            {
                log.Error($"An error occured in login AgentProcess {e.Message}");
            }
        }

        public void AgentLogout(InteractionServerProtocol _ixnServerProtocol)
        {
            try
            {
                RequestAgentLogout request = RequestAgentLogout.Create(null);

                log.LogRequest(request.ToString());

                _ixnServerProtocol.Send(request);
            }
            catch (ProtocolException e)
            {
                log.Error(e.Message);
            }
        }

        void AddMedia(InteractionServerProtocol _ixnServerProtocol)
        {
            try
            {
                RequestAddMedia request = RequestAddMedia.Create();
                request.MediaTypeName = "chat";
                request.Reason = null;

                IMessage ack = _ixnServerProtocol.Request(request);
                if (ack is EventAck)
                {
                    AgentCancelNotReadyForMedia(_ixnServerProtocol);
                }
            }
            catch (ProtocolException e)
            {
                log.Error($"An exception occured {e.Message}");
            }
        }
        public void RemoveMedia(InteractionServerProtocol _ixnServerProtocol)
        {
            try
            {
                RequestRemoveMedia request = RequestRemoveMedia.Create();
                request.MediaTypeName = "chat";
                request.Reason = null;

                log.LogRequest(request.ToString());


                _ixnServerProtocol.Send(request);
            }
            catch (ProtocolException e)
            {
                log.Error(e.Message);
            }
        }

        public void EndChat(InteractionServerProtocol _ixnServerProtocol, string interactionID)
        {
            try
            {
                RequestStopProcessing request = RequestStopProcessing.Create();
                request.InteractionId = interactionID;

                log.LogRequest(request.ToString());

                IMessage message = _ixnServerProtocol.Request(request);
            }
            catch (ProtocolException e)
            {
                log.Error(e.Message);
            }
        }

        public void AgentCancelNotReadyForMedia(InteractionServerProtocol _ixnServerProtocol)
        {
            try
            {
                RequestCancelNotReadyForMedia request = RequestCancelNotReadyForMedia.Create("chat", null);

                IMessage ack = _ixnServerProtocol.Request(request);
                if (ack is EventAck)
                {
                    log.Info($"Bot waiting for new interactions from ixn {_ixnServerProtocol.Endpoint.Host}:{_ixnServerProtocol.Endpoint.Port}");
                }
            }
            catch (ProtocolException e)
            {
                log.Error(e.Message.ToString());
            }
        }

        public void AgentNotReadyForMedia(InteractionServerProtocol _ixnServerProtocol)
        {
            try
            {
                RequestNotReadyForMedia request = RequestNotReadyForMedia.Create("chat", null);
                log.LogRequest(request.ToString());
                _ixnServerProtocol.Send(request);
            }
            catch (ProtocolException e)
            {
                log.Error(e.Message);
            }
        }

        public void AcceptInteraction(InteractionServerProtocol _ixnServerProtocol, BasicChatProtocol _chatServerProtocol, string ticket, string interactionId)
        {
            log.Info($"Accepting new interaction, interaction id: {interactionId}");
            RequestAccept request = RequestAccept.Create(Convert.ToInt32(ticket), interactionId);

            IMessage ack = _ixnServerProtocol.Request(request);
            if (ack is EventAck)
            {
                log.Info(ack.ToString());
                RequestJoin join = RequestJoin.Create(interactionId, Visibility.All, MessageText.Create("Conectado"));
                log.Info(join.ToString());
                _chatServerProtocol.Send(join);
                return;
            }
        }

        public string RegisterChannelList(BasicChatProtocol chatProtocol)
        {
            RequestRegister register = RequestRegister.Create();
            register.UserType = UserType.Agent;
            register.UserNickname = config.Employee;
            register.UserData = new KeyValueCollection();
            register.TimeZoneOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours;

            log.Info(chatProtocol.ToString());
            log.Info($"Requesting Agent Register: {register.ToString()}");

            IMessage msg = chatProtocol.Request(register);

            if (msg is EventRegistered)
            {
                UserId = ((EventRegistered)msg).UserId;
                log.Info($"New user id => {UserId}");
                return UserId;
            }
            else
            {
                log.Error("RequestRegister Fail \r\n" + msg.ToString());
                return "";
            }
        }

        public void UploadFile(BasicChatProtocol chatProtocol, UniversalContactServerProtocol ucsProt, string interactionID, byte[] file)
        {

            RequestAddDocument docReq = RequestAddDocument.Create();
            RequestNotify fileReq = RequestNotify.Create();

            try
            {
                docReq.MimeType = "application/pdf";
                docReq.Content = file;
                docReq.TheName = "dirf.pdf";
                docReq.TheSize = docReq.Content.Length;
                IMessage addDocReply = ucsProt.Request(docReq);
                EventAddDocument addDocEvent = addDocReply as EventAddDocument;

                fileReq.SessionId = interactionID;
                fileReq.NoticeText = NoticeText.Create(NoticeType.SystemCommand, "file-uploaded");
                fileReq.UserData = new KeyValueCollection();
                fileReq.UserData.Add("file-name", docReq.TheName);
                fileReq.UserData.Add("file-size", docReq.TheSize);
                fileReq.UserData.Add("file-source", "ucs");
                fileReq.UserData.Add("file-document-id", addDocEvent.DocumentId);
                fileReq.UserData.Add("file-upload-type", "file-system");
                fileReq.UserData.Add("file-description", "Sample text file.");
                IMessage fileUploadedReply = chatProtocol.Request(fileReq);
                log.Info(fileUploadedReply.ToString());
            }
            catch (Exception e)
            {
                log.Info($"An error occured in UploadFileMethod: {e.Message}");
                throw;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~GenesysMethodsProtocol()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }


}
