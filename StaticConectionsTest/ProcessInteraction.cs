﻿using BotAgent.Arguments;
using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.Commons.Threading;
using Genesyslab.Platform.OpenMedia.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Events;
using Genesyslab.Platform.WebMedia.Protocols;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Events;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Requests;
using Newtonsoft.Json;
using BotAgent.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebSocket4Net;
using Genesyslab.Platform.OpenMedia.Protocols.InteractionServer.Requests.InteractionManagement;
using System.IO;
using Genesyslab.Platform.Contacts.Protocols;

namespace BotAgent
{
    public class ProcessInteractionService
    {
        private static BasicChatProtocol _chatchannel1, _chatchannel2, _chatchannel3, _chatchannel4, _chatchannel5;
        private static UniversalContactServerProtocol _ucschannel;
        private static InteractionServerProtocol _ixnchannel;
        private static GenesysMethodsProtocol _genesysMethods = new GenesysMethodsProtocol();
        private static List<BasicChatProtocol> protocolList = new List<BasicChatProtocol>();
        private static List<GenesysConectionProtocol<BasicChatProtocol>> genesysConnProtList = new List<GenesysConectionProtocol<BasicChatProtocol>>();
        private static ChatRepository _chatRepository = new ChatRepository();
        private static UserIDRepository _userIDRepository = new UserIDRepository();
        private static ItxLogger log = ItxLogger.GetLogger();
        private static ConfigurationService config;
        private static Task ixnTask;
        private static Task ucsTask;
        private static Task chatServerTask;
        private static Task webSocketTask;

        public ProcessInteractionService(ConfigurationService _config)
        {
            config = _config;
        }

        //Endpoint ep;

        public void Start()
        {
            //Teste a chamada invertida - Primeiro estabelece comunicação com os ChatServers e depois recebe interações do IXN.
            log.Info("Starting ITXLabs.BotAgent");
            ixnTask = Task.Factory.StartNew(InitializeIxnProtocol);
            chatServerTask = Task.Factory.StartNew(InitializeChatProtocol);
            webSocketTask = Task.Factory.StartNew(InitializeWebSocketConn);
            ucsTask = Task.Factory.StartNew(InicializeUCSProt);
        }
        public void Stop()
        {
            log.Info($"Stopping ItxLabs BotAgent");
            try
            {
                ixnTask.Dispose();
                chatServerTask.Dispose();
            }
            catch (Exception e)
            {
                log.Error($"error on dispose task ixn: {e}");
            }
        }

        public static void InitializeIxnProtocol()
        {
            GenesysConectionProtocol<InteractionServerProtocol> _genesysIxnServer = new GenesysConectionProtocol<InteractionServerProtocol>(_ixnchannel = new InteractionServerProtocol()
            {
                ClientName = "FrameworkClient",
                ClientType = InteractionClient.AgentApplication,
                Invoker = new MultithreadedInvoker("MultiThreaded", null),
            }, new Endpoint(config.IxnHost, config.IxnPort));

            try
            {
                AssignIxnEvents(_genesysIxnServer);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }
        
        public static void InicializeUCSProt()
        {
            GenesysConectionProtocol<UniversalContactServerProtocol> _ucsChannel1 = new GenesysConectionProtocol<UniversalContactServerProtocol>(_ucschannel = new UniversalContactServerProtocol()
            {
                ClientApplicationType = "AgentApplication",
                ClientName = "FrameworkClient",
                Invoker = new MultithreadedInvoker("MultiThreaded", null)
            }, new Endpoint("10.82.251.27", 6200));
        }

        public static void InitializeChatProtocol()
        {
            GenesysConectionProtocol<BasicChatProtocol> _genesysChatServer1 = new GenesysConectionProtocol<BasicChatProtocol>(_chatchannel1 = new BasicChatProtocol()
            {
                UserType = UserType.Agent,
                UserNickname = "employee",
                PersonId = "employee",
                UserData = new KeyValueCollection(),
                TimeZoneOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours,
                Invoker = new MultithreadedInvoker("MultiThreaded", null),
                AutoRegister = false
            }, new Endpoint(config.ChatHost, config.ChatPort));

            protocolList.Add(_chatchannel1);
            genesysConnProtList.Add(_genesysChatServer1);

            //GenesysConectionProtocol<BasicChatProtocol> _genesysChatServer2 = new GenesysConectionProtocol<BasicChatProtocol>(_chatchannel2 = new BasicChatProtocol()
            //{
            //    UserType = UserType.Agent,
            //    UserNickname = "employee",
            //    PersonId = "employee",
            //    UserData = new KeyValueCollection(),
            //    TimeZoneOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours,
            //    Invoker = new MultithreadedInvoker("MultiThreaded", null),
            //    AutoRegister = false
            //}, new Endpoint("192.168.77.135", 6100));
            //GenesysConectionProtocol<BasicChatProtocol> _genesysChatServer3 = new GenesysConectionProtocol<BasicChatProtocol>(_chatchannel3 = new BasicChatProtocol()
            //{
            //    UserType = UserType.Agent,
            //    UserNickname = "employee",
            //    PersonId = "employee",
            //    UserData = new KeyValueCollection(),
            //    TimeZoneOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours,
            //    Invoker = new MultithreadedInvoker("MultiThreaded", null),
            //    AutoRegister = false
            //}, new Endpoint("192.168.77.136", 6100));
            //GenesysConectionProtocol<BasicChatProtocol> _genesysChatServer4 = new GenesysConectionProtocol<BasicChatProtocol>(_chatchannel4 = new BasicChatProtocol()
            //{
            //    UserType = UserType.Agent,
            //    UserNickname = "employee",
            //    PersonId = "employee",
            //    UserData = new KeyValueCollection(),
            //    TimeZoneOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours,
            //    Invoker = new MultithreadedInvoker("MultiThreaded", null),
            //    AutoRegister = false
            //}, new Endpoint("192.168.77.137", 6100));
            //GenesysConectionProtocol<BasicChatProtocol> _genesysChatServer5 = new GenesysConectionProtocol<BasicChatProtocol>(_chatchannel5 = new BasicChatProtocol()
            //{
            //    UserType = UserType.Agent,
            //    UserNickname = "employee",
            //    PersonId = "employee",
            //    UserData = new KeyValueCollection(),
            //    TimeZoneOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours,
            //    Invoker = new MultithreadedInvoker("MultiThreaded", null),
            //    AutoRegister = false
            //}, new Endpoint("192.168.77.138", 6100));

            try
            {
                foreach (var item in genesysConnProtList)
                {
                    AssignChatEvents(item);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }

        public static void InitializeWebSocketConn()
        {
            try
            {
                BotConnection.CreateWebSocket();
                BotConnection.Listen(WebSocket_EventReceived);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }

        public static void AssignIxnEvents(GenesysConectionProtocol<InteractionServerProtocol> _genesysIxnServer)
        {
            _genesysIxnServer.EventOpened += GenesysIxnServer_EventOpened;
            _genesysIxnServer.EventClosed += GenesysIxnServer_EventClosed;
            _genesysIxnServer.EventReceived += GenesysIxnServer_EventReceived;
            //_genesysIxnServer.EventError += genesysIxnServer_EventError;
        }
        public static void AssignChatEvents(GenesysConectionProtocol<BasicChatProtocol> genesysChatServer)
        {

            genesysChatServer.EventOpened += GenesysChatServer_EventOpened;
            genesysChatServer.EventClosed += GenesysChatServer_EventClosed;
            genesysChatServer.EventReceived += GenesysChatServer_EventReceived;
            //genesysChatServer.EventError += genesysChatServer_EventError;
        }

        public static void GenesysIxnServer_EventClosed(object sender, EventArgs e)
        {
            log.Info($"IXNServer Channel Closed {e.ToString()}");
        }

        static void GenesysIxnServer_EventOpened(object sender, EventArgs e)
        {
            log.Info($"IXNServer Channel Opened {e.ToString()}");

            //using (_genesysMethods = new GenesysMethodsProtocol())
            //{
            //    _genesysMethods.AgentLogin(_ixnchannel);
            //}

            _genesysMethods.AgentLogin(_ixnchannel);
        }
        public static void WebSocket_EventReceived(object sender, EventArgs e)
        {
            try
            {
                log.Info($"Message received from bot");
                var data = JsonConvert.DeserializeObject<MessageWebSocket>(((MessageReceivedEventArgs)e).Message);
                HandleMessage.HandleIncommingWSMessage(data);
            }
            catch (Exception ex)
            {
                log.Error($"An error occured in deserialising message from bot: {ex.Message}");
            }
        }

        static void GenesysIxnServer_EventReceived(object sender, EventArgs e)
        {

            var msg = ((MessageEventArgs)e).Message;
            log.Info($"Event Received from IXN: {msg.ToString()}");

            if (msg is EventInvite eventInvite)
            {
                string interactionID = eventInvite.Interaction.InteractionId;
                string ticketID = eventInvite.TicketId.ToString();
                string ChatHost = Convert.ToString(eventInvite.Interaction.InteractionUserData.GetAsString("ChatServerHost"));

                if (_chatRepository.ContainsKey(interactionID))
                {
                    log.Info($"Already have an bot session with this interaction id, ignoring interaction: {interactionID}");
                    return;
                }
                else
                {
                    //Utilizar a linha abaixo em produção.
                    _genesysMethods.AcceptInteraction(_ixnchannel, protocolList.Find(n => n.Endpoint.Host == ChatHost), ticketID, interactionID);
                    _chatRepository.Add(interactionID, ChatHost);

                    ////Valor cravado para homologação
                    //_genesysMethods.AcceptInteraction(_ixnchannel, protocolList.Find(n => n.Endpoint.Host == "192.168.77.135"), ticketID, interactionID);
                    //_chatRepository.Add(interactionID, ChatHost);
                }
            }
        }

        //static void genesysIxnServer_EventError(object sender, EventArgs e)
        //{
        //    Console.WriteLine($"IXNServer Channel Error occured {e.ToString()}");

        //    _genesysIxnServer.EventOpened -= genesysIxnServer_EventOpened;
        //    _genesysIxnServer.EventClosed -= genesysIxnServer_EventClosed;
        //    _genesysIxnServer.EventError -= genesysIxnServer_EventError;
        //    _genesysIxnServer.EventReceived -= genesysIxnServer_EventReceived;
        //}

        static void GenesysChatServer_EventClosed(object sender, EventArgs e)
        {
            log.Info($"ChatServer Channel Closed");
        }

        static void GenesysChatServer_EventOpened(object sender, EventArgs e)
        {
            log.Info($"ChatServer Channel Opened");
            foreach (var chatProt in protocolList)
            {
               var userID = _genesysMethods.RegisterChannelList(chatProt);
                _userIDRepository.Add(chatProt.Endpoint.Host, userID);
            }
        }

        static void GenesysChatServer_EventReceived(object sender, EventArgs e)
        {
            IMessage msg = ((MessageEventArgs)e).Message;
            log.Info($"Event received from ChatServer");
            log.Info(msg.ToString());

            if (msg.Id == EventSessionInfo.MessageId)
            {
                EventSessionInfo info = (EventSessionInfo)msg;
                log.Info(info.ToString());
                if (info.SessionStatus == SessionStatus.Over)
                {
                    EndChat(info.ChatTranscript.SessionId);
                }
                else
                {
                    HandleMessage.HandleIncommingGenesysMessage(info);
                }
            }
            else
            {
                log.Info(string.Format("EventReceived {0}: {1}", sender != null ? sender.ToString() : "NULL", e != null ? e.ToString() : "NULL"));
            }
        }

        //Pensar em uma forma de tratar os erros de connection
        //static void genesysChatServer_EventError(object sender, EventArgs e)
        //{
        //    log.Error($"ChatServer Channel Error occured {e.ToString()}");

        //    genesysChatServer.EventOpened -= genesysChatServer_EventOpened;
        //    genesysChatServer.EventClosed -= genesysChatServer_EventClosed;
        //    genesysChatServer.EventReceived -= genesysChatServer_EventReceived;
        //    genesysChatServer.EventError -= genesysChatServer_EventError;
        //}

        public static void SendMessageToGenesys(RequestMessage message)
        {
            var ChatHostByID = _chatRepository.Get(message.SessionId.ToString());
            try
            {
                log.Info($"Sending message to user: {message.SessionId}, in ChatServer: {ChatHostByID}");
                protocolList.Find(n => n.Endpoint.Host == ChatHostByID).Send(message);
            }
            catch (Exception exp)
            {
                log.Error($"An error occured in SendMessageToGenesys Method: {exp.Message}");
            }
        }

        public static void SendMessageToWS(MessageWebSocket message)
        {
            try
            {
                log.Info($"Sending message to bot: {message.Id}");
                BotConnection.SendMessageToBot(message);
            }
            catch (Exception exc)
            {
                log.Error($"And error occured in SendMessageToGenesys Method: {exc.Message}");
            }
        }

        public static object RequestToIXN(RequestChangeProperties req)
        {
            try
            {
                var response = _ixnchannel.Request(req);
                return response;
            }
            catch (Exception e)
            {
                log.Error($"An error occured in RequestToIXN Method: {e.Message}");
                return e.Message;
            }
        }

        public static object ReturnAttach(RequestGetInteractionProperties request)
        {
            try
            {
                var response = _ixnchannel.Request(request);
                return response as EventInteractionProperties;
            }
            catch (Exception e)
            {
                log.Error($"An error occured in RequestToIXN Method: {e.Message}");
                return e.Message;
            }
        }

        public static void End(string interactionId, bool desloga)
        {
            EndChat(interactionId);
            if (desloga)
            {
                foreach (string item in _chatRepository.Keys())
                {
                    EndChat(item);
                }

                _genesysMethods.AgentNotReadyForMedia(_ixnchannel);
                _genesysMethods.RemoveMedia(_ixnchannel);
                _genesysMethods.AgentLogout(_ixnchannel);

                if (_ixnchannel.State == ChannelState.Opened)
                {
                    _ixnchannel.Close();
                }
            }
        }

        private static void EndChat(string interactionId)
        {
            if (_chatRepository.ContainsKey(interactionId))
            {
                _genesysMethods.EndChat(_ixnchannel, interactionId);
                RemoveChatFromRepository(interactionId);
            }
            _genesysMethods.AgentCancelNotReadyForMedia(_ixnchannel);
        }
        
        public static void Transfer(string interactionID)
        {
            log.Info("Inicializing Transfer");
            var ChatHostByID = _chatRepository.Get(interactionID);
            try
            {
                ChatTransferService.Execute(interactionID, _userIDRepository.Get(ChatHostByID), interactionID,
                _ixnchannel,
                protocolList.Find(n => n.Endpoint.Host == ChatHostByID),
                () =>
                {
                    log.Info("Received from the Transfer Command");
                    var notice = RequestNotify.Create(interactionID, Visibility.All);
                    notice.NoticeText = NoticeText.Create(NoticeType.Custom, "Transfer");
                    protocolList.Find(n => n.Endpoint.Host == ChatHostByID).Send(notice);
                },
                e =>
                {
                    log.Info("End of the command transfer Sucess");
                });
            }
            catch (Exception e)
            {
                log.Error($"An error occured at transfer {e.Message}");
            }
        }

        public static void RemoveChatFromRepository(string interactionId)
        {
            try
            {
                log.Info($"Removing chat: {interactionId} from the ChatRepository");
                _chatRepository.Remove(interactionId);
                log.Info($"Chat:{interactionId} removed from the ChatRepository");
            }
            catch (Exception e)
            {
                log.Error($"An error occured in RemoveChatFromRepositoryMethod: {e.Message.ToString()}");
            }
        }

        public static void Upload(string interactionID, byte[] file)
        {
            try
            {
                _genesysMethods.UploadFile(protocolList.Find(n => n.Endpoint.Host == _chatRepository.Get(interactionID)), _ucschannel, interactionID, file);
            }
            catch (Exception ex)
            {
                log.Info($"An error occured in Upload File to Chat {ex.Message}");
                throw;
            }
        }
        public static string GetUserIDInfo(string interactionID)
        {
            try
            {
                return _userIDRepository.Get(_chatRepository.Get(interactionID));
            }
            catch (Exception exc)
            {
                log.Info($"An error occured in GetUserIDInfo {exc.Message}");
                return "";
            }
        }
    }
}
