﻿using FX.Configuration;

namespace BotAgent
{
    public class ConfigurationService : AppConfiguration
    {
        public string WS { get; set; }

        public string ClientName { get; set; }

        public string IxnHost { get; set; }

        public int IxnPort { get; set; }

        public string IxnHost_2 { get; set; }

        public int IxnPort_2 { get; set; }

        public string ChatHost { get; set; }

        public int ChatPort { get; set; }

        public string ChatHost_2 { get; set; }

        public int ChatPort_2 { get; set; }

        public string ChatHost_3 { get; set; }

        public int ChatPort_3 { get; set; }

        public string ChatHost_4 { get; set; }

        public int ChatPort_4 { get; set; }

        public string ChatHost_5 { get; set; }

        public int ChatPort_5 { get; set; }

        public int TenantId { get; set; }

        public string Employee { get; set; }

        public string Place { get; set; }

        public string Queue { get; set; }

        public string ServiceName { get; set; }

        public override string ToString()
        {
            return $"{nameof(ConfigurationService)}( " +
                 $"{nameof(WS)}: {WS}," +
                 $"{nameof(ClientName)}: {ClientName}," +
                 $"{nameof(IxnHost)}: {IxnHost}," +
                 $"{nameof(IxnPort)}: {IxnPort}," +
                 $"{nameof(IxnHost_2)}: {IxnHost_2}," +
                 $"{nameof(IxnPort_2)}: {IxnPort_2}," +
                 $"{nameof(ChatHost)}: {ChatHost}," +
                 $"{nameof(ChatPort)}: {ChatPort}," +
                 $"{nameof(ChatHost_2)}: {ChatHost_2}," +
                 $"{nameof(ChatPort_2)}: {ChatPort_2}," +
                 $"{nameof(ChatHost_3)}: {ChatHost_3}," +
                 $"{nameof(ChatPort_3)}: {ChatPort_3}," +
                 $"{nameof(ChatHost_4)}: {ChatHost_4}," +
                 $"{nameof(ChatPort_4)}: {ChatPort_4}," +
                 $"{nameof(ChatHost_5)}: {ChatHost_5}," +
                 $"{nameof(ChatPort_5)}: {ChatPort_5}," +
                 $"{nameof(TenantId)}: {TenantId}," +
                 $"{nameof(Employee)}: {Employee}," +
                 $"{nameof(Place)}: {Place}," +
                 $"{nameof(Queue)}: {Queue}," +
                 $"{nameof(ServiceName)}: {ServiceName}," +
                $" );";
        }
    }
}